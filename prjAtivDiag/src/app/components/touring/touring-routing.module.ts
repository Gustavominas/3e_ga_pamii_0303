import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TouringPage } from './touring.page';

const routes: Routes = [
  {
    path: '',
    component: TouringPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TouringPageRoutingModule {}
