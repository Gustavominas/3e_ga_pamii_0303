import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadastroClientesPageRoutingModule } from './cadastro-clientes-routing.module';

import { CadastroClientesPage } from './cadastro-clientes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadastroClientesPageRoutingModule
  ],
  declarations: [CadastroClientesPage]
})
export class CadastroClientesPageModule {}
