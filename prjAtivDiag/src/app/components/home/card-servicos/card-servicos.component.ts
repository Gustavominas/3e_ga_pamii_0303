import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-servicos',
  templateUrl: './card-servicos.component.html',
  styleUrls: ['./card-servicos.component.scss'],
})
export class CardServicosComponent implements OnInit {

  constructor() { }
  slideOpts = {
    autoplay: true,
    slidesPerView: 1,
    initialSlide: 1,
    speed: 400
  };
  ngOnInit() {}

}
