import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { NavbarComponent } from './navbar/navbar.component';
import { CarouselComponent } from './carousel/carousel.component';
import { CardMotoComponent } from './card-moto/card-moto.component';
import { CardServicosComponent } from './card-servicos/card-servicos.component';
import { CompreComponent } from './compre/compre.component';
import { FooterComponent } from './footer/footer.component';
import { DestaquesComponent } from './destaques/destaques.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule
  ],
  declarations: [
    HomePage,
    NavbarComponent,
    CarouselComponent,
    CardMotoComponent,
    CardServicosComponent,
    CompreComponent,
    FooterComponent,
    DestaquesComponent
  ]
})
export class HomePageModule {}
