import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-moto',
  templateUrl: './card-moto.component.html',
  styleUrls: ['./card-moto.component.scss'],
})
export class CardMotoComponent implements OnInit {

  constructor() { }
  slideOpts = {
    autoplay: true,
    slidesPerView: 2,
    initialSlide: 1,
    speed: 400
  };
  ngOnInit() {}

}
