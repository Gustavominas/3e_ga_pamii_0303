import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent implements OnInit {

  constructor() { }
  slideOpts = {
    autoplay: true,
    initialSlide: 1,
    speed: 400
  };
  
  ngOnInit() {}

}
