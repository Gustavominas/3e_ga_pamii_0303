import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AcompanhamentoPedidosPageRoutingModule } from './acompanhamento-pedidos-routing.module';

import { AcompanhamentoPedidosPage } from './acompanhamento-pedidos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AcompanhamentoPedidosPageRoutingModule
  ],
  declarations: [AcompanhamentoPedidosPage]
})
export class AcompanhamentoPedidosPageModule {}
