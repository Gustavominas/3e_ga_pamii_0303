import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StreetPage } from './street.page';

const routes: Routes = [
  {
    path: '',
    component: StreetPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StreetPageRoutingModule {}
